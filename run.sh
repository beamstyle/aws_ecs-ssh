#!/bin/bash
set -e

# Set some variables to be used later
SSH_ROOT_PASSWORD=${SSH_ROOT_PASSWORD}
SSH_ADMIN_USERNAME=${SSH_ADMIN_USERNAME}
SSH_ADMIN_PASSWORD=${SSH_ADMIN_PASSWORD}
SSH_HOME_DIRECTORY=${SSH_HOME_DIRECTORY}

# Sets password for user "root"
echo "root:$SSH_ROOT_PASSWORD" | chpasswd

# Creates and sets password for user "$SSH_ADMIN_USERNAME"
useradd $SSH_ADMIN_USERNAME -d /home/$SSH_ADMIN_USERNAME

mkdir -p /home/$SSH_ADMIN_USERNAME
chmod -R 700 /home/$SSH_ADMIN_USERNAME
chown -R $SSH_ADMIN_USERNAME:$SSH_ADMIN_USERNAME /home/$SSH_ADMIN_USERNAME

mkdir -p /home/$SSH_ADMIN_USERNAME/.ssh
chmod 700 /home/$SSH_ADMIN_USERNAME/.ssh
chown $SSH_ADMIN_USERNAME:$SSH_ADMIN_USERNAME /home/$SSH_ADMIN_USERNAME/.ssh

echo "$SSH_ADMIN_USERNAME:$SSH_ADMIN_PASSWORD" | chpasswd
chsh -s /bin/bash $SSH_ADMIN_USERNAME

# Grant permission for user "$SSH_ADMIN_USERNAME" to access to $SSH_HOME_DIRECTORY
chown -R $SSH_ADMIN_USERNAME:$SSH_ADMIN_USERNAME $SSH_HOME_DIRECTORY

# Sets default SSH directory for users "root" and "$SSH_ADMIN_USERNAME"
echo "cd $SSH_HOME_DIRECTORY" >> ~/.bashrc

# Fixes SSH terminal display error (esp. when using VI) for users "root" and "$SSH_ADMIN_USERNAME"
echo "shopt -s checkwinsize" >> ~/.bashrc 
echo "set nocompatible" >> ~/.vimrc
echo "set backspace=2" >> ~/.vimrc

# Copying .profile and .bashrc from user "root" to user "$SSH_ADMIN_USERNAME"
cp ~/.profile /home/$SSH_ADMIN_USERNAME/.profile
cp ~/.bashrc /home/$SSH_ADMIN_USERNAME/.bashrc
cp ~/.vimrc /home/$SSH_ADMIN_USERNAME/.vimrc

# Runs the SSH daemon
/usr/sbin/sshd -D
